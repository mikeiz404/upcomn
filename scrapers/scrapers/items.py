from scrapy.item import Item, Field
from scrapy.contrib.loader.processor import Join, TakeFirst

from scrapers.processors import DateProcessor, TimeProcessor, DefaultProcessor

# EventItem #################################################################
class EventItem(Item):
   title = Field()
   category = Field()
   poster_url = Field()
   start_date = Field( output_processor=DateProcessor() )
   start_time = Field( output_processor=TimeProcessor() )
   end_date = Field( output_processor=DateProcessor() )
   end_time = Field( output_processor=TimeProcessor() )
   duration = Field()
   meta_cost = Field()
   meta_website = Field()
   meta_ticketsite = Field()
   meta_facebookevent = Field()
   meta = Field()
   description = Field()

   location_name = Field( output_processor=TakeFirst() )
   location_room = Field()
   location_building = Field()
   location_college = Field()
   location_city = Field()
   location_state = Field()
   location_country = Field()
   location_street = Field()
   location_address = Field()
   location_lat = Field()
   location_lng = Field()
   
   default_city = Field() #Field( output_processor=DefaultProcessor('default_city') )
   default_state = Field() #Field( output_processor=DefaultProcessor('default_state') )
   default_country = Field() #Field( output_processor=DefaultProcessor('default_country') )

   # TODO Add debug fields
   #d_crawler, d_timestamp, d_url
#############################################################################

# LocationItem ##############################################################
class LocationItem(Item):
   name = Field()
   room = Field()
   building = Field()
   college = Field()
   city = Field()
   state = Field()
   street = Field()
   address = Field()
   lat = Field()
   lng = Field()
#############################################################################
