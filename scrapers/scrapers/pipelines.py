# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html
from scrapy.conf import settings

import urllib2
import json
import datetime
from geopy import geocoders 
from geopy.geocoders.google import GQueryError
import re
import exceptions
import unicodedata


class ScrapersPipeline(object):
    def process_item(self, item, spider):
        return item


class NormalizeDateTimePipeline(object):
    def process_item( self, item, spider ):
        item['datatime']


class DbExportPipeline(object):
    def __init__( self ):
        self.url = settings['DBEXPORTPIPELINE_URL']

    def process_item( self, item, spider ):
        # convert to json
        #TODO move out into datetime normalizer pioeline
        json_data = json.dumps(dict(item))
        post_data = json_data.encode('utf-8')
        # construct post
        headers = {}
        headers['Content-Type'] = 'application/json'
        # post
        request = urllib2.Request(self.url, post_data, headers)
        response_json = urllib2.urlopen(request)
        response = json.loads(response_json.read())
        if response.get('status') != 'ok':
            message = 'STATUS: %s\\n INFO: %s' % (response.get('status'), response.get('status_description'))
            raise Exception('status: ', message)
        return item


class CalculateDurationPipeline(object):
    def process_item( self, item, spider ):
        if item.get('duration') == None:
            # calculate duration if possible
            start_date = item.get('start_date')
            end_time = item.get('end_time')
            start_time = item.get('start_time')
            if start_time != None and end_time != None and start_date != None:
                start_time_dt = datetime.datetime.strptime(start_time, settings['TIME_OUTPUT_FORMAT'])
                end_time_dt = datetime.datetime.strptime(end_time, settings['TIME_OUTPUT_FORMAT'])
                if end_time_dt >= start_time_dt:
                    duration_delta = end_time_dt - start_time_dt
                else: # end_time_dt < start_time_dt
                    # assume that the event carries over to the next day
                    duration_delta = end_time_dt - start_time_dt + datetime.timedelta(hours=24)
                seconds_total = duration_delta.seconds
                seconds = seconds_total % 60
                minutes_total = seconds_total / 60
                minutes = minutes_total % 60
                hours = minutes_total / 60
                duration_time = datetime.time(hours, minutes, seconds)
                item['duration'] = duration_time.strftime(settings['DELTA_OUTPUT_FORMAT'])
            else:
                print 'CalculateDurationPipeline: start_time, end_time, or start_date not set'
        return item
       
        
class GeocodePipeline(object):
    def __init__( self ):
        self.geocoder = geocoders.Google()
        
    def process_item( self, item, spider ):
        address = item.get('location_address')
        if address:
            try:
                try:
                    place, (lat, lng) = self.geocoder.geocode(address)
                except exceptions.ValueError as e:
                    # multiple results returned, guess address with city, state, country appended
                    city = item.get('default_city')
                    state = item.get('default_state')
                    country = item.get('default_country')
                    if city and state and country:
                        address_guess = '%s, %s, %s, %s' % (address, city, state, country)
                        print ">><< GUESSING: ", address_guess
                        place, (lat, lng) = self.geocoder.geocode(address_guess)
                        print ">><< GUESSING RESULT: ", place
                    else:
                        return item
            except GQueryError as e:
                print 'GeocodePipeline: %s' % e
                return item
            # state, city, street
            streetCityStateCountry = place.split(', ')
            if len(streetCityStateCountry) < 4:
                # no street, geocode results are not specific enough
                return item
            if len(streetCityStateCountry) >= 1:
                country = streetCityStateCountry[-1]
            if len(streetCityStateCountry) >= 2:
                stateZip = streetCityStateCountry[-2]
                state = stateZip.split(' ')[0]
                item['location_state'] = state
            if len(streetCityStateCountry) >= 3:
                city = streetCityStateCountry[-3]
                item['location_city'] = city
            if len(streetCityStateCountry) >= 4:
                street = streetCityStateCountry[-4]
                item['location_street'] = street
            # lat
            if not item.get('location_lat'):
                item['location_lat'] = lat
            # lng
            if not item.get('location_lng'):
                item['location_lng'] = lng
            print ">><< place: ", place
        return item
        
            
class SanitizeTextPipeline(object):
    sanitizer_blank = re.compile(r'\t|\r|\xa0')
    sanitizer_newline = re.compile(r'\n')
    sanitizer_space = re.compile(u'\u2022')
    ignore = settings['SANITIZETEXTPIPELINE_IGNORE']
        
    def process_item( self, item, spider ):
        for key, val in item.items():
            if key not in self.ignore and val != None:
                val = self.sanitizer_blank.sub('', val)
                val = self.sanitizer_newline.sub('\n', val)
                val = self.sanitizer_space.sub(' ', val)
                item[key] = val.strip()
        return item                    
        
        
class EncodeTextPipeline(object):
    def process_item( self, item, spider ):
        for key, value in item.items():
            if value is not None:
                item[key] = unicode(value).encode('utf-8', 'ignore')
        return item
        
        
#TODO: refactor into processor
class DefaultPipeline(object):
    def process_item( self, item, spider ):
        if item.get('location_city') is None:
            item['location_city'] = item.get('default_city')
        if item.get('location_state') is None:
            item['location_state'] = item.get('default_state')
        if item.get('location_country') is None:
            item['location_country'] = item.get('default_country')
        return item
        
        
class MetaPipeline(object):
    def process_item( self, item, spider ):
        meta_keys = ['meta_cost', 'meta_facebookevent', 'meta_website']
        meta = {}
        for key in meta_keys:
            value = item.get(key)
            if value:
                meta[key.replace('meta_', '')] = value
        if meta:
            item['meta'] = meta
        return item
