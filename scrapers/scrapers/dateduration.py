from datetime import timedelta, datetime
import time

# TODO default to dateutil.parser.parse to automatically pull out dates

class DateDuration(object):
    def __init__( self, start, stop=None, duration=None):
        """
        Inits object from either start and, stop datetime xor duration timedelta
        objects or string, string format tuples

        >>> start = datetime(year=2012, month=4, day=15)
        >>> stop = datetime(year=2012, month=4, day=15, hour=4, minute=30)
        >>> duration = timedelta(days=0, hours=4, minutes=30)

        >>> dd = DateDuration(start=start, stop=stop)
        >>> print dd
        <DateDuration start=2012-04-15 00:00:00 duration=4:30:00>
        >>> dd = DateDuration(start=start, duration=duration)
        >>> print dd
        <DateDuration start=2012-04-15 00:00:00 duration=4:30:00>

        >>> dd = DateDuration(start=('Friday, April 20, 2012, 12:00 PM', '%A, %B %d, %Y, %I:%M %p'), duration=('1:00' ,'%H:%M'))
        >>> print dd
        <DateDuration start=2012-04-20 12:00:00 duration=1:00:00>
        >>> dd = DateDuration(start=('Friday, April 20, 2012, 12:00 PM', '%A, %B %d, %Y, %I:%M %p'), stop=('Friday, April 20, 2012, 1:00 PM', '%A, %B %d, %Y, %I:%M %p'))
        >>> print dd
        <DateDuration start=2012-04-20 12:00:00 duration=1:00:00>
        >>> dd = DateDuration(start=('Friday, April 20, 2012, 12:00 PM', '%A, %B %d, %Y, %I:%M %p'), stop=('1:00 PM','%I:%M %p'))
        >>> print dd
        <DateDuration start=2012-04-20 12:00:00 duration=1:00:00>
        """
        # either stop or duration is defined but not both
        assert((stop is None) != (duration is None))
        # convert to respective datetime and timedelta objects
        start = self._convert_to_datetime(start)
        stop = self._convert_to_datetime(stop)
        duration = self._convert_to_timedelta(duration)
        # calculate duration if necessary
        if stop is not None:
            # if date is set to default values, assume we should use start
            # date values
            if stop.year==1900 and stop.month==1 and stop.day==1:
                stop = datetime(start.year, start.month, start.day,\
                                stop.hour, stop.minute, stop.second)
            assert(stop > start)
            duration = stop - start
        # store values
        self.start = start
        self.duration = duration

    def _convert_to( self, obj, fn ):
        if isinstance(obj, tuple):
            data, format = obj
            return fn(data, format)
        else:
            return obj

    def _convert_to_datetime( self, obj ):
        return self._convert_to(obj, datetime.strptime)

    def _convert_to_timedelta( self, obj):
        def convert( data, format ):
            tTime = time.strptime(data,format)
            return timedelta(hours=tTime.tm_hour, minutes=tTime.tm_min, seconds=tTime.tm_sec)
        return self._convert_to(obj, convert)

    def __repr__( self ):
        return "<DateDuration start=%s duration=%s>" % (self.start, self.duration)


class DateDurationProcessor(object):
    def __init__( self, start, stop=None, duration=None ):
        self.start_format = start
        self.stop_format = stop
        self.duration_format = duration

    def __call__( self, values ):
        start_data = values[0]
        if self.stop_format is not None:
            stop_data = values[1]
            return DateDuration(start=(start_data, self.start_format), stop=(stop_data, self.stop_format))
        elif self.duration_format is not None:
            duration_data = values[1]
            return DateDuration(start=(start_data, self.start_format), duration=(duration_data, self.duration_format))

if __name__ == "__main__":
    import doctest
    doctest.testmod()
