from scrapy.contrib.loader import XPathItemLoader
from scrapy.contrib.loader.processor import TakeFirst
from scrapy.utils.misc import arg_to_iter
from scrapy.utils.python import flatten
from scrapy.contrib.loader.common import wrap_loader_context
from scrapy.utils.python import flatten

from scrapers.overides import extract_regex


# EventItemLoader ###########################################################
class ItemLoader(XPathItemLoader):
    default_output_processor = TakeFirst()

    def get_value(self, value, *processors, **kw):
        regex = kw.get('re', None)
        group = kw.get('group', None)
        if regex:
            value = arg_to_iter(value)
            value = flatten([extract_regex(regex, x, group) for x in value])

        for proc in processors:
            if value is None:
                break
            proc = wrap_loader_context(proc, self.context)
            value = proc(value)
        return value
#############################################################################

