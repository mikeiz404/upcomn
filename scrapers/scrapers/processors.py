from datetime import datetime
import re
import exceptions

class DateProcessor(object):
    def __call__(self, texts, loader_context):
        if not texts:
            return None
        try:
            dt = datetime.strptime(texts[0], loader_context['date_parse_format']).date()
            return dt.strftime(loader_context['date_output_format'])
        except exceptions.ValueError as e:
            print 'DateProcessor: ', e
            return None
        
class TimeProcessor(object):
    def __call__(self, texts, loader_context):
        if not texts:
            return None
        try:
            dt = datetime.strptime(texts[0], loader_context['time_parse_format']).time()
            return dt.strftime(loader_context['time_output_format'])
        except exceptions.ValueError as e:
            print 'TimeProcessor: ', e
            return None
    
class SanitizeProcessor(object):
    sanitizer = re.compile(r'\n|\t|\r|\xa0')
    def __call__(self, texts, loader_context):
        for text in texts:
            yield str(self.sanitizer.sub('', text).strip())
            
class DefaultProcessor(object):
    def __init__( self, context_key ):
        self.context_key = context_key
        
    def __call__( self, values, loader_context ):
        print ">><< DefaultProcessor: value=", value
        print ">><< DefaultProcessor: context=", loader_context
        if not values:
            return loader_context.get(self.context_key)
        else:
            return values
