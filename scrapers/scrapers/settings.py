# Scrapy settings for scrapers project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'scrapers'
BOT_VERSION = '1.0'

SPIDER_MODULES = ['scrapers.spiders']
NEWSPIDER_MODULE = 'scrapers.spiders'
USER_AGENT = '%s/%s' % (BOT_NAME, BOT_VERSION)

DATE_OUTPUT_FORMAT = '%Y-%m-%d'
TIME_OUTPUT_FORMAT = '%H:%M:%S'
DELTA_OUTPUT_FORMAT = '%H:%M:%S'

DBEXPORTPIPELINE_URL = 'http://localhost:8081/api/event/add'

SANITIZETEXTPIPELINE_IGNORE = ['poster_url',
                               'start_date',
                               'start_time',
                               'end_date',
                               'end_time',
                               'duration',
                               'location_lat',
                               'location_lng',
                               'meta',
]

ITEM_PIPELINES = [
    'scrapers.pipelines.MetaPipeline',
    'scrapers.pipelines.CalculateDurationPipeline',
    'scrapers.pipelines.SanitizeTextPipeline',
    'scrapers.pipelines.EncodeTextPipeline',
    'scrapers.pipelines.GeocodePipeline',
    'scrapers.pipelines.DefaultPipeline',
    'scrapers.pipelines.DbExportPipeline',
]
