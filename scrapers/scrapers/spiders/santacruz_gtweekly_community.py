from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.loader.processor import TakeFirst, Join
import re
from urlparse import urlparse
from scrapy.conf import settings

from scrapers.extractors import SoupLinkExtractor
from scrapers.items import EventItem, LocationItem
from scrapers.itemLoaders import ItemLoader
from scrapers.dateduration import DateDurationProcessor
from scrapers.processors import DateProcessor, TimeProcessor, SanitizeProcessor


# UcscBsoeSpider ############################################################
class SantacruzGoodtimessantacruzCommunitySpider(CrawlSpider):
    name = 'santacruz_gtweekly_community'
    allowed_domains = ['gtweekly.com']
    start_urls = ['http://www.gtweekly.com/index.php/santa-cruz-arts-entertainment-lifestyles/events-calendar-activities-santa-cruz.html?option=com_jevents&task=cat.listevents&offset=1&category_fv=74&Itemid=18',
                  'http://www.gtweekly.com/index.php/santa-cruz-arts-entertainment-lifestyles/events-calendar-activities-santa-cruz.html?option=com_jevents&task=cat.listevents&offset=1&category_fv=47&Itemid=18',
                  'http://www.gtweekly.com/index.php/santa-cruz-arts-entertainment-lifestyles/events-calendar-activities-santa-cruz.html?option=com_jevents&task=cat.listevents&offset=1&category_fv=42&Itemid=18',
                  'http://www.gtweekly.com/index.php/santa-cruz-arts-entertainment-lifestyles/events-calendar-activities-santa-cruz.html?option=com_jevents&task=cat.listevents&offset=1&category_fv=49&Itemid=18',

                 ]

    rules = (
        # follow event info pages
        Rule(SoupLinkExtractor(allow=r'./eventdetail/\d+/\d+/.+\.html'), callback='parse_event', follow=False),
        # follow next links
        Rule(SoupLinkExtractor(allow=r'/eventsbycategory/-.html\?.*(offset=\d+.*|start=\d+.*){2}.*')),
    )

    re_location_nameaddress1 = re.compile(r'(?P<name>[\w\d&! ]+),.*')
    re_location_nameaddress2 = re.compile(r'(?P<name>[\w\d&! ]+)$')
    re_location_nameaddress3 = re.compile(r'(?P<name>[\w\d&! ]+) (\d+.*)')
    re_startend_datetime = re.compile(r'.*\w+, (?P<start_date>\w+ \d+, \d+),.*(?P<start_time>\d+:\d+(am|pm)).*(?P<end_time>\d+:\d+(am|pm)).*')
    category_map = {'74':'FoodWine', '47':'Music', '42':'Arts', '49':'Outdoors'}

    def parse_event( self, response ):
        hxs = HtmlXPathSelector(response)
        hxs_event = hxs.select('//div[@id="ja-current-content"]//table[@class="contentpaneopen"]')
        el = ItemLoader(EventItem(), hxs_event)
        el.context['date_parse_format'] = '%B %d, %Y'
        el.context['time_parse_format'] = '%I:%M%p'
        el.context['date_output_format'] = settings['DATE_OUTPUT_FORMAT']
        el.context['time_output_format'] = settings['TIME_OUTPUT_FORMAT']
        el.add_value('default_city', 'Santa Cruz')
        el.add_value('default_state', 'CA')
        el.add_value('default_country', 'USA')
        el.add_xpath('title', './tr[1]/td/text()',  SanitizeProcessor())
        category_id = urlparse(response.url).path.split('/')[-2]
        el.add_value('category', self.category_map[category_id])
        el.add_xpath('start_date', './tr[3]/td/table/tr/td[contains(@class, "repeat")]/text()', SanitizeProcessor(), Join(), re=self.re_startend_datetime, group='start_date')
        el.add_xpath('start_time', './tr[3]/td/table/tr/td[contains(@class, "repeat")]/text()', SanitizeProcessor(), Join(), re=self.re_startend_datetime, group='start_time')
        el.add_xpath('end_time', './tr[3]/td/table/tr/td[contains(@class, "repeat")]/text()', SanitizeProcessor(), Join(), re=self.re_startend_datetime, group='end_time')
        el.add_xpath('meta_cost', './tr[6]/td/text()')
        el.add_xpath('description', './tr[4]/td/text()', SanitizeProcessor())
        el.add_xpath('location_name', './tr[5]/td/text()[preceding-sibling::b[1][contains(text(), "Location : ")]]', re=self.re_location_nameaddress1, group='name')
        el.add_xpath('location_name', './tr[5]/td/text()[preceding-sibling::b[1][contains(text(), "Location : ")]]', re=self.re_location_nameaddress2, group='name')
        el.add_xpath('location_name', './tr[5]/td/text()[preceding-sibling::b[1][contains(text(), "Location : ")]]', re=self.re_location_nameaddress3, group='name')
        el.add_xpath('location_address', './tr[5]/td/text()[preceding-sibling::b[1][contains(text(), "Location : ")]]')
        el.add_value('meta_website', response.url)
        yield el.load_item()
#############################################################################
