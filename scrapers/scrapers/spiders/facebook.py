from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.loader.processor import TakeFirst, Join
from scrapy.http import Request
import re
from urlparse import urlparse
from scrapy.conf import settings
import json

from scrapers.extractors import SoupLinkExtractor
from scrapers.items import EventItem, LocationItem
from scrapers.itemLoaders import ItemLoader
from scrapers.dateduration import DateDurationProcessor
from scrapers.processors import DateProcessor, TimeProcessor, SanitizeProcessor


# Facebookpider ############################################################
class FacebookSpider(CrawlSpider):
    allowed_domains = ['graph.facebook.com']
    fb_access_token = 'AAACEdEose0cBAIFZCR5J1XAUXkzp6mVVpyc6EsYOmWKPydlbmgqdJvHPIeWzzErGJCSLx1zUaXRlqyRoWvBLIXs6FnrG5c0whyekPGgZDZD'

    def parse( self, response ):
        data = json.loads(response.body)
        if 'data' in data:
            for event in data['data']:
                yield Request(url='https://graph.facebook.com/%s' % event['id'], meta={'action':'event'}, callback=self.parse_handler)

    def parse_handler( self, response):
        action = response.meta['action']
        if action == 'event':
            event = json.loads(response.body)
            item = self.parse_event(response, event, EventItem())
            yield Request(url='https://graph.facebook.com/%s' % event['venue']['id'], meta={'action':'location', 'event_id':event['id'], 'item':item}, callback=self.parse_handler, dont_filter=True)
        elif action == 'location':
            location = json.loads(response.body)
            item = self.parse_location(response, location, response.meta['item'])
            yield Request(url='https://graph.facebook.com/fql?q=SELECT pic_big FROM event WHERE eid=%s' % response.meta['event_id'], meta={'action':'picture', 'item':item}, callback=self.parse_handler)
        elif action == 'picture':
            data = json.loads(response.body)
            if 'data' in data:
                pictures = data['data'][0]
                item = self.parse_pictures(response, pictures, response.meta['item'])
                item = self.parse_return(item)
                yield item
           
    def parse_event( self, response, event, item ):
        el = ItemLoader(item, response)
        el.context['date_parse_format'] = '%Y-%m-%dT%H:%M:%S'
        el.context['time_parse_format'] = '%Y-%m-%dT%H:%M:%S'
        el.context['date_output_format'] = settings['DATE_OUTPUT_FORMAT']
        el.context['time_output_format'] = settings['TIME_OUTPUT_FORMAT']
        el.add_value('title', ' '.join([word.capitalize() for word in event['name'].split(' ')]))
        el.add_value('start_date', event['start_time'])
        el.add_value('start_time', event['start_time'])
        el.add_value('end_date', event['end_time'])
        el.add_value('end_time', event['end_time'])
        el.add_value('description', event['description'])
        return el.load_item()

    def parse_location( self, response, location, item ):
        el = ItemLoader(item, response)
        el.add_value('location_name', location['name'])
        el.add_value('location_street', location['location']['street'])
        el.add_value('location_city', location['location']['city'])
        el.add_value('location_state', location['location']['state'])
        el.add_value('location_country', location['location']['country'])
        el.add_value('location_address', '%s, %s, %s, %s' % (location['location']['street'], location['location']['city'], location['location']['state'], location['location']['country']) )
        el.add_value('location_lat', location['location']['latitude'])
        el.add_value('location_lng', location['location']['longitude'])
        return el.load_item()
        
    def parse_pictures( self, response, pictures, item ):
        el = ItemLoader(item, response)
        el.add_value('poster_url', pictures['pic_big'])
        return el.load_item()
        
    def parse_return( self, item ):
        return item
