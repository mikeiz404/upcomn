from scrapers.items import EventItem
from scrapers.itemLoaders import ItemLoader
from scrapers.spiders.facebook import FacebookSpider


# Facebookpider ############################################################
class SantacruzFacebookCatalystSpider(FacebookSpider):
    name = 'santacruz_facebook'
    start_urls = ['https://graph.facebook.com/theabbeysc/events?access_token=%s' % FacebookSpider.fb_access_token,
                  'https://graph.facebook.com/catalystclub/events?access_token=%s' % FacebookSpider.fb_access_token,
                  'https://graph.facebook.com/moesalley/events?access_token=%s' % FacebookSpider.fb_access_token,
                  'https://graph.facebook.com/MOTIVSC/events?access_token=%s' % FacebookSpider.fb_access_token,
                  'https://graph.facebook.com/SantaCruzBeachBoardwalk/events?access_token=%s' % FacebookSpider.fb_access_token,
                 ]

    def parse_event( self, response, event, item ):
        item['meta_facebookevent'] = 'http://facebook.com/%s' % event['id']
        item['meta_website'] = 'http://facebook.com/%s' % event['venue']['id']
        return FacebookSpider.parse_event(self, response, event, item)

    def parse_return( self, item ):
        if item.get('location_name') == 'The Catalyst Club':
            item['category'] = 'music'
        elif item.get('location_name') == 'The Abbey':
            item['category'] = 'music'
        elif item.get('location_name') == 'Moe\'s Alley, Santa Cruz':
            item['category'] = 'music'
        elif item.get('location_name') == 'MOTIV':
            item['category'] = 'music'
        elif item.get('location_name') == 'Santa Cruz Beach Boardwalk':
            if 'band' in item.get('title').lower():
                item['category'] = 'music'
            elif 'movie' in item.get('title').lower():
                item['category'] = 'film'
        return item
