from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
import re

from scrapers.items import EventItem
from scrapers.itemLoaders import ItemLoader
from scrapers.dateduration import DateDurationProcessor


# UcscBsoeSpider ############################################################
class UcscBsoeSpider(CrawlSpider):
    name = 'ucsc_soe'
    allowed_domains = ['soe.ucsc.edu']
    start_urls = ['http://www.soe.ucsc.edu/events']

    rules = (
        Rule(SgmlLinkExtractor(allow=r'/events/event/\d+$'), callback='parse_event', follow=False),
    )

    re_dateduration = re.compile(r'(?P<start>\w+, \w+ \d{1,2}, \d{4}, \d{1,2}:\d{2} (?:AM|PM)) to (?P<duration>\d{1,2}:\d{2} (?:AM|PM))')
    re_location = re.compile(r'Location: (?P<building>[\w\d\s]+), Room (?P<room>[\w\d\s]+)')

    def parse_event( self, response ):
        hxs = HtmlXPathSelector(response)
        hxs_event = hxs.select('//div[@id="content"]/div[@class="soe_events"]')
        l = ItemLoader(EventItem(), hxs_event)
        l.add_xpath('title', '//h1[@id="title"]/text()')
        #dateDurationProcessor = DateDurationProcessor(start='%A, %B %d, %Y, %I:%M %p', stop='%I:%M %p')
        #l.add_xpath('dateduration', './p/em/text()', dateDurationProcessor, re=self.re_dateduration)
        l.add_xpath('location_building', './p/em/text()', re=self.re_location, group='building')
        l.add_xpath('location_room', './p/em/text()', re=self.re_location, group='room')
        return l.load_item()
#############################################################################
