from pyramid.response import Response
from pyramid.view import view_config
from pyramid.renderers import get_renderer
from sqlalchemy.sql.expression import asc, or_
from pyramid.httpexceptions import HTTPNotFound

import parsedatetime.parsedatetime as pdt
import parsedatetime.parsedatetime_consts as pdc
import datetime
import calendar
from collections import defaultdict
import hashlib

from ..models import (
    DBSession,
    Event,
    Location,
    Category,
)

def datetimeFromString( s ):
    c = pdt.Calendar()
    result, what = c.parse( s )

    dt = None

    # what was returned (see http://code-bear.com/code/parsedatetime/docs/)
    # 0 = failed to parse
    # 1 = date (with current time, as a struct_time)
    # 2 = time (with current date, as a struct_time)
    # 3 = datetime
    if what in (1,2):
        # result is struct_time
        dt = datetime.datetime( *result[:6] )
    elif what == 3:
        # result is a datetime
        dt = result
    else:
        dt = None
    return dt

#class EventGroups(object):
#    def __init__( self, query ):
#        self.groups = defaultdict(list)
#        self.today = datetime.datetime.now()
#        self.this_month = self.today.date() + datetime.timedelta(days=1)
#        self.limit = 10000;
#        if self.this_month.month != self.today.date().month:
#            # today is the end of the month, no need for this category
#            self.this_month = None
#        # by today
#        results = query.filter(Event.start_date == self.today.date() and Event.end_time() >= self.today.time()).limit(self.limit)
#        self.limit = self.limit - len(results)
#        self.groups['today'] = results
#        # by this month
#        if self.this_month is not None:
#            # month is not today
#            results = query.filter(Event.start_date.month == self.this_month.month).limit(self.limit)
#            self.limit = self.limit - len(results)
#            self.groups['this_month'] = results
#        # by months
#        # group remaining results by month
#        results = query.filter(Event.start_date.month > self.this_month.month).limit(self.limit)
#        
#            
#        #loop incrementing each month untill self.limit is 0
#        

class EventGroups(object):
    def __init__( self, events=[] ):
        self.groups = defaultdict(list)
        self.today = datetime.datetime.now()
        self.this_month = self.today.date() + datetime.timedelta(days=1)
        if self.this_month.month != self.today.date().month:
            # today is the end of the month, no need for this category
            self.this_month = None
        self.add_events(events)
            
    def add_event( self, event ):
        # by today
        if event.start_date == self.today.date() and event.end_time() >= self.today.time():
            group_today = DateRangeGroup(self.today.date(), start_time=self.today.time(), end_date=self.today.date())
            self.groups[group_today].append(event)
        # by this month
        elif event.start_date.month == self.this_month.month:
            _, last_day = calendar.monthrange(self.this_month.year, self.this_month.month)
            end_date = datetime.date(self.this_month.year, self.this_month.month, last_day)
            group_this_month = DateRangeGroup(self.this_month, end_date=end_date)
            self.groups[group_this_month].append(event)
        # by months
        else:
            month = calendar.month_name[event.start_date.month]
            start_date = datetime.date(event.start_date.year, event.start_date.month, 1)
            _, last_day = calendar.monthrange(start_date.year, start_date.month)
            end_date = datetime.date(event.start_date.year, event.start_date.month, last_day)
            group_month = DateRangeGroup(start_date, end_date=end_date)
            self.groups[group_month].append(event)
            
    def add_events( self, events ):
        for event in events:
            self.add_event( event )
            
    def get_groups( self ):
        for group, events in sorted(self.groups.items()):
            group.extend(events)
            yield group


class DateRangeGroup(list):
    def __init__( self, start_date, start_time=None, end_date=None, end_time=None):
        self.start_date = start_date
        self.start_time = start_time
        self.end_date = end_date
        self.end_time = end_time
        
    def can_append( event ):
        if (event.start_date >= self.start_date)\
        and (self.start_time and event.start_time >= self.start_time)\
        and (self.end_date and event.start_date <= self.end_date)\
        and (self.end_time and event.start_time <= self.end_time):
            return True
        else:
            return False
        
    def append( event ):
        if self.can_append(event):
            self.append(event)
        else:
            raise 'Cannot add event'
            
    def __hash__( self ):
        return hash('%s%s%s%s' % (self.start_date, self.start_time, self.end_date, self.end_time))
            
    def __comp__( self, other ):
        if isinstance(other, DateRangeGroup)\
        and self.start_date == other.start_date\
        and self.start_time == other.start_time\
        and self.end_date == other.end_date\
        and self.end_time == other.end_time:
            return True
        else:
            return False
    def __eq__(self, other):
        return ((self.start_date, self.start_time, self.end_date, self.end_time) ==
                (other.start_date, other.start_time, other.end_date, other.end_time))
    def __lt__(self, other):
        return ((self.start_date, self.start_time, self.end_date, self.end_time) <
                (other.start_date, other.start_time, other.end_date, other.end_time))

    
    def _last_day_of_month( self, date):
        _, last_day = calendar.monthrange(date.year, date.month)
        return last_day
    
    def name( self ):
        today = datetime.date.today()
        _, last_day = calendar.monthrange(self.start_date.year, self.start_date.month)
        
        print ">><< ", self.start_date, self.end_date, today
        if self.start_date == today and self.end_date == today:
            # today
            return 'Today'
        elif self.start_date.year == self.end_date.year == today.year\
        and self.start_date.month == self.end_date.month == today.month\
        and self.start_date.day == today.day + 1\
        and self.end_date.day == last_day:
            # this month
            return 'This Month'
        elif self.start_date.day == 1\
        and self.start_date.year == self.end_date.year\
        and self.start_date.month == self.end_date.month\
        and self.end_date.day == self._last_day_of_month(self.start_date):
            if self.start_date.year == today.year:
                return self.start_date.strftime('%B')
            else:
                return self.start_date.strftime('%B (%Y)')
        #elif 
        # week
        # custom
        else:
            return '%s to %s' % (self.start_date, self.end_date)
    
class Group(object):
    def __init__( self, group_type, name=None ):
        self.name = name
        self.type = group_type
        self.events = []
        
    def add( self, event ):
        self.events.append(event)
        
    def __cmp__( self, other ):
        if isinstance(other, Group):
            return self.type == other.type and self.name == other.name
        else:
            return False


@view_config( route_name='events', renderer='web:templates/browse.pt' )
def events( request ):
    response = {}
    response['city_name'] = 'Santa Cruz'
    event_templates = get_renderer('web:templates/events.pt').implementation().macros
    view = request.params.get('view')
    if view == 'list':
        response['results_template'] = event_templates['list']
        response['results_view'] = 'list'
    elif view == 'poster':
        response['results_template'] = event_templates['poster']
        response['results_view'] = 'poster'
    elif view == 'map':
        response['results_template'] = event_templates['map']
        response['results_view'] = 'map'
    else:
        response['results_template'] = event_templates['list']
        response['results_view'] = 'list'

    response['view'] = view

    print ">><< ", request.GET

    # categories
    categories = DBSession.query(Category).all()
    response['categories'] = categories
    none_checked = True
    for category in categories:
        category.checked = request.params.get('category_%s' % category.id) == 'on'
        if category.checked:
            none_checked = False
    if none_checked:
        # default to all checked if none checked
        for category in categories:
            category.checked = True
    # datetime
    start_date = None
    start_time = None
    end_date = None
    end_time = None
    start_date_text = request.params.get('start_date')
    start_time_text = request.params.get('start_time')
    end_date_text = request.params.get('end_date')
    end_time_text = request.params.get('end_time')
    if start_date_text:
        dt = datetimeFromString(start_date_text)
        if dt:
            start_date = dt.strftime('%Y-%m-%d')
    if start_time_text:
        dt = datetimeFromString(start_time_text)
        if dt:
            start_time = dt.strftime('%H:%M:%S')
    if end_date_text:
        dt = datetimeFromString(end_date_text)
        if dt:
            end_date = dt.strftime('%Y-%m-%d')
    if end_time_text:
        dt = datetimeFromString(end_time_text)
        if dt:
            end_time = dt.strftime('%H:%M:%S')
            
    # day of week
    date_week = request.params.get('date_week') == 'on'
    date_monday = request.params.get('date_monday') == 'on'
    date_tuesday = request.params.get('date_tuesday') == 'on'
    date_wednesday = request.params.get('date_wednesday') == 'on'
    date_thursday = request.params.get('date_thursday') == 'on'
    date_friday = request.params.get('date_friday') == 'on'
    date_weekend = request.params.get('date_weekend') == 'on'
    date_saturday = request.params.get('date_saturday') == 'on'
    date_sunday = request.params.get('date_sunday') == 'on'
    if not date_monday and not date_tuesday and not date_wednesday and not date_thursday and not date_friday and not date_saturday and not date_sunday:
        date_monday = True
        date_tuesday = True
        date_wednesday = True
        date_thursday = True
        date_friday = True
        date_saturday = True
        date_sunday = True
    if date_saturday and date_sunday:
        date_weekend = True
    else:
        date_weekend = False
    if date_monday and date_tuesday and date_wednesday and date_thursday and date_friday:
        date_week = True
    else:
        date_week = False
            
    # geo
    geo_ne = {'lat':request.params.get('geo_ne_lat'), 'lng':request.params.get('geo_ne_lng')}
    geo_sw = {'lat':request.params.get('geo_sw_lat'), 'lng':request.params.get('geo_sw_lng')}
 
    filter = {'start_date': start_date,
              'start_time': start_time,
              'end_date': end_date,
              'end_time': end_time,
              'date_week': date_week,
              'date_monday': date_monday,
              'date_tuesday': date_tuesday,
              'date_wednesday': date_wednesday,
              'date_thursday': date_thursday,
              'date_friday': date_friday,
              'date_weekend': date_weekend,
              'date_saturday': date_saturday,
              'date_sunday': date_sunday,
              }
    response['filter_query'] = filter
        
    events = DBSession.query(Event).join(Location)
    category_list = []
    for category in categories:
        if category.checked:
            category_list.append(Event.category_id == category.id)
    events = events.filter(or_(*category_list))
#TODO: Fix error which is reported by SqlAlchemy
    #events = events.filter(query)
#    if geo_ne['lat'] and geo_ne['lng'] and geo_sw['lat'] and geo_sw['lng']:
#        events = events.filter(Event.location.lat >= geo_ne[lat]\
#                               and Event.location.lat <= geo_sw[lat]\
#                               and Event.location.lng <= geo_ne[lng]\
#                               and Event.location.lng <= geo_sw[lng])

    date_list = []
    if date_monday:
        print 'm'
        date_list.append(Event.start_day == 'Monday')
    if date_tuesday:
        print 't'
        date_list.append(Event.start_day == 'Tuesday')
    if date_wednesday:
        print 'w'
        date_list.append(Event.start_day == 'Wednesday')
    if date_thursday:
        print 'th'
        date_list.append(Event.start_day == 'Thursday')
    if date_friday:
        print 'f'
        date_list.append(Event.start_day == 'Friday')
    if date_saturday:
        print 'sa'
        date_list.append(Event.start_day == 'Saturday')
    if date_sunday:
        print 'su'
        date_list.append(Event.start_day == 'Sunday')
    print date_list
    events = events.filter(or_(*date_list))

    print len(events.all())
    if start_date:
        events = events.filter(Event.start_date >= start_date)
    else:
        events = events.filter(Event.start_date >= datetime.date.today())
        if not start_time:
            events = events.filter((Event.start_date == datetime.date.today() and Event.start_time >= datetime.datetime.now().time()) or Event.start_date >= datetime.date.today())
    if start_time:
        events = events.filter(Event.start_time >= start_time)
    if end_date:
        events = events.filter(Event.start_date <= end_date)
    if end_time:
        events = events.filter(Event.start_time <= end_time)
    events = events.order_by(asc(Event.start_date), asc(Event.start_time), asc(Event.duration)).all()
    
    # TODO: Fix
    # hackish implementation to filter geolocation
#    if geo_ne['lat'] and geo_ne['lng'] and geo_sw['lat'] and geo_sw['lng']:
#        new_events = []
#        for event in events:
#            print event.location.lat
#            print event.location.lng
#            if( event.location.lat >= geo_ne['lat']\
#                and event.location.lat <= geo_sw['lat']\
#                and event.location.lng <= geo_ne['lng']\
#                and event.location.lng <= geo_sw['lng']\
#              ):
#                new_events.append(event)
#        events = new_events
    print len(events)
    event_groups = EventGroups(events)
    response['events'] = events
    response['groups'] = event_groups.get_groups()

    return response

@view_config( route_name='events_map', renderer='web:templates/browse.pt' )
def events_map( request ):

    
    results_map_template = get_renderer('web:templates/events.pt').implementation().macros['list']
    return {'results_template': results_map_template,
            'results_view': 'map',
            'current_query': request.GET,
            'city_name': 'Santa Cruz',
            'events': events,
            
            }

@view_config( route_name='events_poster', renderer='web:templates/browse.pt' )
def events_poster( request ):
    events = DBSession.query(Event).all()
    results_poster_template = get_renderer('web:templates/events.pt').implementation().macros['poster']
    return {'results_template': results_poster_template,
            'results_view': 'poster',
            'current_query': request.GET,
            'city_name': 'Santa Cruz',
            'events':events,
            }
            
@view_config( route_name='event_details', renderer='web:templates/event_details.pt' )
def events_poster( request ):
    event_id = request.matchdict.get('event_id')
    event = DBSession.query(Event).filter(Event.id == event_id).first()
    if event:
        return {'event': event}
    else:
        return HTTPNotFound('Event not found')
