from pyramid.response import Response
from pyramid.view import view_config

from sqlalchemy.exc import DBAPIError
from sqlalchemy import func
import datetime
import json

from ..models import (
    DBSession,
    Event,
    Location,
    Category,
    )

@view_config(route_name='api_event_add', renderer='json')
def api_event_add( request ):
    # TODO: validate with http://www.formencode.org/en/latest/Validator.html#introduction
    response = {'status':'unknown error'}
    if request.method == 'POST':
        # try to parse the post as a json object
        if request.json_body:
            print request.json_body
            # event
            event_title = request.json_body.get('title')
            event_start_time = request.json_body.get('start_time')
            event_start_date = request.json_body.get('start_date')
            event_description = request.json_body.get('description')
            event_end_time = request.json_body.get('end_time')
            event_duration = request.json_body.get('duration')
            event_poster_url = request.json_body.get('poster_url')
            # category
            category_name = request.json_body.get('category')
            # location
            location_name = request.json_body.get('location_name')
            location_street = request.json_body.get('location_street')
            location_city = request.json_body.get('location_city')
            location_state = request.json_body.get('location_state')
            location_address = request.json_body.get('location_address')
            location_lat = request.json_body.get('location_lat')
            location_lng = request.json_body.get('location_lng')
            # meta
            meta_cost = request.json_body.get('meta_cost')
            meta_facebookevent = request.json_body.get('meta_facebookevent')
            meta_website = request.json_body.get('meta_website')
            # lookup/insert location
            location = Location.find_like()
            if not location:
                # location does not exist, so create it
                location = Location(name=location_name, street=location_street, city=location_city, state=location_state, address=location_address, lat=location_lat, lng=location_lng)
                print ">><< ADDING NEW LOCATION: ", location
                try:
                    DBSession.add(location)
                except DBAPIError:
                    response['status'] = 'could not add location'
                    response['status_description'] = str(location)
                    return response
            # lookup/insert category
            category = Category.find_like(category_name)
            if not category:
                response['status'] = 'could not find category'
                response['status_description'] = str(category)
                return response
            # lookup/insert event
            event = Event.find_like(title=event_title, category=category, location=location, start_time=event_start_time, start_date=event_start_date, description=event_description, end_time=event_end_time, duration=event_duration, poster_url=event_poster_url, meta_cost=meta_cost, meta_facebookevent=meta_facebookevent, meta_website=meta_website)
            if not event:
                # event does not exist, so create it
                event = Event(title=event_title, category=category, location=location, start_time=event_start_time, start_date=event_start_date, description=event_description, end_time=event_end_time, duration=event_duration, poster_url=event_poster_url, meta_cost=meta_cost, meta_facebookevent=meta_facebookevent, meta_website=meta_website)
                try:
                    DBSession.add(event)
                except DBAPIError:
                    response['status'] = 'could not add event'
                    response['status_description'] = str(event)
                    return response
                response['status'] = 'ok'
    return response
        
