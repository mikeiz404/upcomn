/* NavigationMap **************************************************************/
function NavigationMap( map_canvas, html_location,
                        html_street, html_distance,
                        html_measure, form_ne_lat, form_ne_lng,
                        form_sw_lat, form_sw_lng
                      )
{
    this.html_street = html_street;
    this.html_distance = html_distance;
    this.html_measure = html_measure;
    this.html_location = html_location;
    this.form_ne_lat = form_ne_lat;
    this.form_ne_lng = form_ne_lng;
    this.form_sw_lat = form_sw_lat;
    this.form_sw_lng = form_sw_lng;
    //TODO: lookup city_location from form
    this.city_location = {'lat': '1', 'lng': '1'};
    this.city_address = {'city':'Santa Cruz', 'state':'CA', 'country': 'USA'};
    this.geo_location = null;
    this.geo_address = null;
    this.default_location = null;
    this.map_options = { zoom: 15,
                         minZoom: 10,
                         disableDefaultUI: true,
                         mapTypeId: google.maps.MapTypeId.ROADMAP,
                       };
    this.map = new google.maps.Map(map_canvas[0], this.map_options);
    this.geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(this.map, 'dragend', $.proxy(function()
    {
        this._map_changed_handler();
    }, this));
    
    this._no_geo_location = function ()
    {
          this.geo_location = null;
          this.default_location = this.city_location;
    }
    
    this.find_default_location = function( callback )
    {
        // lookup current geolocation
        if(navigator.geolocation)
        {
            navigator.geolocation.getCurrentPosition( $.proxy(function(position)
            {
                this.geo_location = { 'lat':position.coords.latitude,
                                      'lng':position.coords.longitude,
                                    };
                // check if geocode_location is in  city_location
                // reverse geocode
                var position = new google.maps.LatLng(this.geo_location['lat'], this.geo_location['lng']);
                this.geocoder.geocode({'latLng': position}, $.proxy(function(results, status)
                {
                    var result = results[0];
                    if (status == google.maps.GeocoderStatus.OK && result)
                    {
                        // compare city, state, country
                        parts = result.formatted_address.split(',');
                        this.geo_address = { 'city': $.trim(parts[1]),
                                             'state': $.trim($.trim(parts[2]).split(' ')[0]),
                                             'country': $.trim(parts[3]),
                                            };
                        if( this.geo_address['city'] == this.city_address['city'] &&
                            this.geo_address['state'] == this.city_address['state'] &&
                            this.geo_address['country'] == this.city_address['country']
                          )
                        {
                            this.default_location = this.geo_location;
                            callback();
                        }
                        else // not $(this.geo_address).is(this.city_address)
                        {
                            // fallback to city_location
                            this.default_location = this.city_location;
                            callback();
                        }
                        
                    }
                    else // status != google.maps.GeocoderStatus.OK || !result
                    {
                        // fallback to city_location
                        this.default_location = this.city_location;
                        callback();
                    }
                }, this));
            },  this),
            $.proxy( function()
            {
                // fallback to city_location
                this._no_geo_location();
                callback();
            }, this));
        }
        else
        {
          // Browser doesn't support Geolocation
          this._no_geo_location();
          callback();
        }
    }
    
    this.move_to_default_location = function()
    {
        var position = new google.maps.LatLng(this.default_location['lat'], this.default_location['lng']);
        this._move_to_location(position);
        this.map.setZoom(this.map_options['zoom']);
        this.html_location.hide();
    }
    
    this.move_to_location = function( lat, lng )
    {
        var position = new google.maps.LatLng(lat, lng);
        this._move_to_location(position);
    }
    
    this._move_to_location = function( position )
    {
        this.map.setCenter( position );
        this._update_form();
        this._update_html();
        this.html_location.show();
    }
    
    this._map_changed_handler = function()
    {
        var position = this.map.getCenter();
        this._move_to_location(position);
    };
    
    this._update_form = function()
    {
        var bounds = this.map.getBounds();
        var ne = bounds.getNorthEast();
        var sw = bounds.getSouthWest();
        this.form_ne_lat.val(ne.lat());
        this.form_ne_lng.val(ne.lng());
        this.form_sw_lat.val(sw.lat());
        this.form_sw_lng.val(sw.lng());
    };
    
    // convert distance in meters to miles or feet
    this._round_to_miles = function( distance )
    {
        var distance_miles = Math.round(distance * 0.000621371192);
        if( distance_miles > 0 )
        {
            distance = distance_miles;
            if( distance == 1 )
            {
                measure = 'mile';
            }
            else // distance > 1
            {
                measure = 'miles';
            }
        }
        else // distance_miles < 0
        {
            distance = 0.5;
            measure = 'miles';
        }
        return {'distance': distance, 'measure': measure};
    };
    
    this._update_html = function()
    {
        var position = this.map.getCenter();
        var bounds = this.map.getBounds();
        var ne = bounds.getNorthEast();
        var sw = bounds.getSouthWest();
        var radius_meters = google.maps.geometry.spherical.computeDistanceBetween(ne, sw) / 2;
        var distance_measure = this._round_to_miles(radius_meters);
        var distance = distance_measure['distance'];
        var measure = distance_measure['measure'];
        this.geocoder.geocode({'latLng': position}, $.proxy( function(results, status)
        {
            if (status == google.maps.GeocoderStatus.OK)
            {
                var street = results[0].formatted_address.split(',')[0]
                
                this.html_street.html(street);
                this.html_distance.html(distance);
                this.html_measure.html(measure);
            }
            else
            {
                // ignore
                this.html_street.html('??');
                this.html_distance.html(distance);
                this.html_measure.html(measure);
            }
        }, this));
    };
    
    this.add_event = function( lat, lng, image, title, url)
    {
        var myLatLng = new google.maps.LatLng(lat, lng);
        var eventMarker = new google.maps.Marker({
            position: myLatLng,
            map: this.map,
            icon: image,
            title: title,
        });
        google.maps.event.addListener(eventMarker, 'click', function()
        {
            $.fancybox.open([
            {
                href : url,
                type: 'iframe',
                'padding':'1',
                'height':'80%',
            }]);
        });
        google.maps.event.addListener(eventMarker, 'mouseover', function()
        {
        });
    }
}

/*
google.maps.event.addListener(map, 'idle', function(event) {
    var bounds = map.getBounds();

    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();

    var viewportPoints = [
        ne, new google.maps.LatLng(ne.lat(), sw.lng()),
        sw, new google.maps.LatLng(sw.lat(), ne.lng()), ne
    ];

    if (viewportBox) {
        viewportBox.setPath(viewportPoints);
    } else {
        viewportBox = new google.maps.Polyline({
            path: viewportPoints,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 4 
        });
        viewportBox.setMap(map);
    };
});
geocoder = new google.maps.Geocoder();
}
*/

/* CategoryMenu ***************************************************************/
function CategoryMenu()
{
    this.categories = [];
    this.set = function( category, show )
    {
        var icon = this.categories[category]['icon'];
        if( show == true )
        {
            icon.show();
        }
        else
        {
            icon.hide();
        }
    }
    
    this.update = function()
    {
        
    }

    this.add = function( name, icon )
    {
        this.categories[name] = {'icon':icon};
    }
}
/*
    this.groups = [];
    this.add_group = function( name, categories)
    {
        this.groups[name] = categories;
    }
*/

/* DayOfWeekMenu ***************************************************************/
function DayOfWeekMenu( week, monday, tuesday, wednesday, thursday, friday, weekend, saturday, sunday )
{
    this.week = week
    this.monday = monday
    this.tuesday = tuesday
    this.wednesday = wednesday
    this.thursday = thursday
    this.friday = friday
    this.weekend = weekend
    this.saturday = saturday
    this.sunday = sunday
    
    this._change_callback = function( event )
    {
        current = event.data
        if( current == this.week )
        {
            checked = current.is(':checked')
            this.monday.attr('checked', checked);
            this.tuesday.attr('checked', checked);
            this.wednesday.attr('checked', checked);
            this.thursday.attr('checked', checked);
            this.friday.attr('checked', checked);
        }
        else if( current == this.weekend )
        {
            checked = current.is(':checked')
            this.saturday.attr('checked', checked);
            this.sunday.attr('checked', checked);
        }
        else
        {
            if( this.monday.is(':checked') && this.tuesday.is(':checked') && this.wednesday.is(':checked') && this.thursday.is(':checked') && this.friday.is(':checked') )
            {
                this.week.attr('checked', true);
            }
            else if( !this.monday.is(':checked') && !this.tuesday.is(':checked') && !this.wednesday.is(':checked') && !this.thursday.is(':checked') && !this.friday.is(':checked') )
            {
                this.week.attr('checked', false);
            }
            if( this.saturday.is(':checked') && this.sunday.is(':checked') )
            {
                this.weekend.attr('checked', true);
            }
            else if( !this.saturday.is(':checked') && !this.sunday.is(':checked') )
            {
                this.weekend.attr('checked', false);
            }
        }
    };
    
    this.week.change(this.week, $.proxy(this._change_callback, this));
    this.monday.change(this.monday, $.proxy(this._change_callback, this));
    this.tuesday.change(this.tuesday, $.proxy(this._change_callback, this));
    this.wednesday.change(this.wednesday, $.proxy(this._change_callback, this));
    this.thursday.change(this.thursday, $.proxy(this._change_callback, this));
    this.friday.change(this.friday, $.proxy(this._change_callback, this));
    this.weekend.change(this.weekend, $.proxy(this._change_callback, this));
    this.saturday.change(this.saturday, $.proxy(this._change_callback, this));
    this.sunday.change(this.sunday, $.proxy(this._change_callback, this));
}

/* FilterMenu *****************************************************************/

function FilterMenu( menu_container )
{
    this.menu_container = menu_container;
    this.menus = [];
    this.current = null;
    
    // click( name )
    this.click = function( name )
    {
        if(this.current != name)
        {
            this.show(name);
        }
        else // this.current == name
        {
            this.close();
        }
    }
    
    // show( name )
    this.show = function( name )
    {
        this.current = name;
        // show the current menu item and hide the rest
        for(var menu_name in this.menus)
        {
            var menu_item = this.menus[menu_name]['menu_item'];
            if(menu_name == name)
            {
                menu_item.show();
            }
            else // current_menu != menu
            {
                menu_item.hide();
            }
        }
        this.menu_container.show();
    }
    
    // close()
    this.close = function()
    {
        this.current = null;
        this.menu_container.hide();
    }
    
    // add( name, menu, menu_item )
    this.add = function( name, menu, menu_item )
    {
        this.menus[name] = {'menu':menu, 'menu_item':menu_item};
    }
}

