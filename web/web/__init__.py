from pyramid.config import Configurator
from sqlalchemy import engine_from_config

from .models import DBSession

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    config = Configurator(settings=settings)
    config.add_static_view('static', 'static', cache_max_age=3600)
    # home
    config.add_route('home', '/')
    # api
    config.add_route('api_event_add', '/api/event/add')
#    config.add_route('api_events_list', '/api/events/list') 
#    # event
    config.add_route('events', '/events')
    config.add_route('event_details', '/e{event_id}')
    config.add_route('events_list', '/events/list')
    config.add_route('events_map', '/events/map')
    config.add_route('events_poster', '/events/poster')
    config.scan()
    return config.make_wsgi_app()

