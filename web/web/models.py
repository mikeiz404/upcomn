from datetime import datetime
import datetime
from xml.sax.saxutils import quoteattr
import json

from sqlalchemy import (
    Column,
    Float,
    Integer,
    Text,
    Date,
    Time,
    Enum,
    ForeignKey,
    )
from sqlalchemy import func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    relationship,
    )
from zope.sqlalchemy import ZopeTransactionExtension
from pyramid.url import route_url

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()

class UniqueBase( object ):
    def __new__( cls, *args, **kwds ):
        found = cls.find_like(*args, **kwds)
        if found:
            return found
        else:
            return object.__new__(cls)

class Event(Base):
    __tablename__ = 'events'
    id = Column(Integer, primary_key=True)
    title = Column(Text, nullable=False)
    category_id = Column(Integer, ForeignKey('categories.id'), nullable=False)
    category = relationship('Category')
    start_date = Column(Date, nullable=False)
    start_day = Column(Text, nullable=False)
    start_time = Column(Time, nullable=False)
    duration = Column(Time, nullable=False)
    poster_url = Column(Text)
    description = Column(Text)
    meta_cost = Column(Text)
    meta_website = Column(Text)
    meta_facebookevent = Column(Text)
    
    location_id = Column(Integer, ForeignKey('locations.id'), nullable=False)
    location = relationship('Location')
    _day_of_week_string = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

    def __init__( self, title, category, location, start_time, start_date, description, end_time=None, duration=None, poster_url=None, meta_cost=None, meta_facebookevent=None, meta_website=None ):
        self.title = title
        self.category = category
        self.location = location
        self.start_time = datetime.datetime.strptime(start_time, '%H:%M:%S').time()
        self.start_date = datetime.datetime.strptime(start_date, '%Y-%m-%d').date()
        if duration is not None:
            self.duration = datetime.datetime.strptime(duration, '%H:%M:%S').time()
        else: # duration is None
            # calculate duration
            start_time_dt = datetime.datetime.strptime(start_time, '%H:%M:%S')
            end_time_dt = datetime.datetime.strptime(end_time, '%H:%M:%S')
            if end_time_dt >= start_time_dt:
                duration_delta = end_time_dt - start_time_dt
            else: # end_time_dt < start_time_dt
                # assume that the event carries over to the next day
                duration_delta = end_time_dt - start_time_dt + datetime.timedelta(hours=24)
            seconds_total = duration_delta.seconds
            seconds = seconds_total % 60
            minutes_total = seconds_total / 60
            minutes = minutes_total % 60
            hours = minutes_total / 60
            self.duration = datetime.time(hours, minutes, seconds)
        self.start_day = self._day_of_week_string[self.start_date.weekday()]
        self.description = description
        self.poster_url= poster_url
        self.meta_cost = meta_cost
        self.meta_website = meta_website
        self.meta_facebookevent = meta_facebookevent
        
    @staticmethod
    def find_like( *args, **kws ):
        return None
        
    def start_time_human( self ):
        return self.start_time.strftime('%I:%M%p').strip('0').lower()[:-1]
        
    def start_date_human( self ):
        return self.start_date.strftime('%b %d/%y')
    
    def end_time( self ):
        duration_delta = datetime.timedelta(seconds=self.duration.hour*60*60+self.duration.minute*60+self.duration.second)
        start_datetime = datetime.datetime(2012, 1, 1, self.start_time.hour, self.start_time.minute, self.start_time.second)
        return (start_datetime + duration_delta).time()
        
    def end_time_human( self ):
        return self.end_time().strftime('%I:%M%p').strip('0').lower()[:-1]
        
    def time_range_human( self ):
        if self.duration.hour == self.duration.minute == self.duration.second == 0 :
            return self.start_time_human()
        else:
            return '%s - %s' % (self.start_time_human(), self.end_time_human())
            
    def url( self ):
        return 'http://localhost:8081/e%i' % self.id
        
    def title_safe( self ):
        return self.title.replace('\'', '\\\'')

class Location(Base):
    __tablename__ = 'locations'
    id = Column(Integer, primary_key=True)
    #type = Column(Enum('on_campus', 'off_campus'))
    name = Column(Text, nullable=False)
    street = Column(Text, nullable=True)
    city = Column(Text, nullable=True)
    state = Column(Text(2), nullable=True)
    address = Column(Text, nullable=False)
    #building = Column(Text)
    #room = Column(Text)
    lat = Column(Float)
    lng = Column(Float)
    
    def __init__( self, name, street, city, state, address, lat, lng ):
        self.name = name
        self.street = street
        self.city = city
        self.state = state
        self.address = address
        self.lat = lat
        self.lng = lng
    
    def geo( self ):
        geo = {}
        if self.lat:
            geo['lat'] = self.lat
        else:
            # TODO: Replace with self.city.lat
            geo['lat'] = 1
        if self.lng:
            geo['lng'] = self.lng
        else:
            # TODO: Replace with self.city.lng
            geo['lng'] = 1
        return geo
    
    @staticmethod
    def find_like( *args, **kws ):
        return None
    
class Category(Base):
    __tablename__ = 'categories'
    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    position = Column(Integer, default=0)
    url = Column(Text, nullable=False)
    
    def __init__( self, name, position, url ):
        self.name = name
        self.position = position
        self.url = url
        self.checked = False
        
    def image_url( self ):
        return '/%s' % self.url
        
    @staticmethod
    def find_like( name, position=None, url=None ):
        return DBSession.query(Category).filter(func.lower(Category.name)==func.lower(name)).first()
